def main():
    marubatsu = Marubatsu(5, 'X', '@')
    marubatsu.game()

class Marubatsu(object):
    def __init__(self, size, p1_piece, p2_piece):
        self.field_size = size
        self.p1_piece = p1_piece
        self.p2_piece = p2_piece
        self.field = self.gen_field()

    def game(self):
        print('Player 1: {0:s}'.format(self.p1_piece))
        print('Player 2: {0:s}'.format(self.p2_piece))
        self.p()
        for i in range(self.field_size * self.field_size):
            print('Player {0:d} input coordinate'.format(i % 2 + 1))
            x, y = Marubatsu.input_coordinate()
            while self.put_piece(x, y, i % 2) is not 0:
                print('Do not put your enter coordinate.')
                print('Player {0:d} input coordinate'.format(i % 2 + 1))
                x, y = Marubatsu.input_coordinate()

            self.p()
            if 4 in self.count(x, y, i % 2):
                print('Player {0:d} win!!'.format(i % 2 + 1))
                break
            
    @staticmethod
    def input_coordinate():
        str = input()
        try:
            x, y = list(map(int, str.split(',')))[:2]
        except (ValueError, TypeError):
            print("Error")
            x, y = Marubatsu.input_coordinate()

        return (x, y)

    def get_piece(self, player):
        if player is 0:
            return self.p1_piece
        else:
            return self.p2_piece

    def put_piece(self, x, y, player):
        if x > 0 and x <= self.field_size and y > 0 and y <= self.field_size:
            if self.field[y][x] in '*' + self.p1_piece + self.p2_piece:
                return 1
            else:
                self.field[y][x] = self.get_piece(player)
                return 0
        else:
            return 1
        
    def gen_field(self):
        field = [['*']*(self.field_size+2) for i in range(self.field_size + 2)]
        for i in range(1, self.field_size+1):
            for j in range(1, self.field_size+1):
                field[i][j] = ' '
        return field

    def count(self, x0, y0, player):
        num_list = [0]*4
        piece = self.get_piece(player)

        def sub_judge(x, y, xx, yy, n):
            # print('x:{0:d}, y:{1:d}, xx:{2:d}, yy:{3:d}, x+xx:{4:d}, y+yy:{5:d}, n:{6:d}, s:{7:s}'.format(x, y, xx, yy, x+xx, y+yy, n, field[y+yy][x+xx]))
            # print('x:{0:d}, y:{1:d}, xx:{2:d}, yy:{3:d}, n:{4:d}'.format(x, y, xx, yy, n))
            if self.field[y + yy][x + xx] is not piece:
                return n
            else:
                return sub_judge(x+xx, y+yy, xx, yy, n+1)

        num_list[0] += sub_judge(x0, y0, 1, 0, 0)
        num_list[0] += sub_judge(x0, y0, -1, 0, 0)
        num_list[1] += sub_judge(x0, y0, 0, 1, 0)
        num_list[1] += sub_judge(x0, y0, 0, -1, 0)
        num_list[2] += sub_judge(x0, y0, -1, 1, 0)
        num_list[2] += sub_judge(x0, y0, 1, -1, 0)
        num_list[3] += sub_judge(x0, y0, 1, 1, 0)
        num_list[3] += sub_judge(x0, y0, -1, -1, 0)

        return num_list

    def p(self):
        def horizontal_line():
            str = ''
            for n in range(self.field_size + 1):
                str += '-+'
            return str
        
        # Header
        print(' |', end='')
        for n in range(self.field_size):
            print('{0:d}|'.format(n+1), end='')
        print('  x')

        print(horizontal_line())

        for i, m in enumerate(self.field[1:-1]):
            print('{0:d}|'.format(i+1), end='')
            for n in m[1:-1]:
                print(n+'|', end='')
            print()
            print(horizontal_line())

        print('y')

if __name__ == '__main__':
    main()
